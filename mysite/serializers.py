from rest_framework import serializers 
from mysite.models import category
 
 
class CategorySerializer(serializers.ModelSerializer):
 
    class Meta:
        model = category
        fields = ('id',
                  'name',
                  'added_on',
                  'image')